# `botofu.swf.parser`

A dumb translation of the 
[SWF](https://wwwimages2.adobe.com/content/dam/acom/en/devnet/pdf/swf-file-format-spec.pdf) 
and [AVM2](https://wwwimages2.adobe.com/content/dam/acom/en/devnet/pdf/avm2overview.pdf) 
standards into C++.


