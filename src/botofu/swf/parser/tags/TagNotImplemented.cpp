#include "botofu/swf/parser/tags/TagNotImplemented.h"

void TagNotImplemented::display(std::ostream &os) const {
    os << "TagNotImplemented()";
}