#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_UTILS_MAKE_UNIQUE_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_UTILS_MAKE_UNIQUE_H

#include <memory>

// From https://en.cppreference.com/w/cpp/memory/unique_ptr/make_unique
// note: this implementation does not disable this overload for array types
template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args &&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_UTILS_MAKE_UNIQUE_H
