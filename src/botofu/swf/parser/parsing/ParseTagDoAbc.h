#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGDOABC_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGDOABC_H

#include <istream>                                              // std::istream
#include <memory>                                               // std::shared_ptr

#include "botofu/swf/parser/SwfReader.h"
#include "botofu/ios/types.h"
#include "botofu/swf/parser/tags/TagBase.h"
#include "botofu/swf/parser/parsing/ParseTagBase.h"

struct ParseTagDoAbc final : public ParseTagBase {

    virtual ~ParseTagDoAbc() = default;

    std::shared_ptr<TagBase> operator()(SwfReader &reader, uint32 /*size*/) override;
};

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGDOABC_H
