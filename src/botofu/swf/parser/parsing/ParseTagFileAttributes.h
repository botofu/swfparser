#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGFILEATTRIBUTES_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGFILEATTRIBUTES_H

#include <memory>

#include "botofu/ios/types.h"
#include "botofu/swf/parser/tags/TagBase.h"
#include "botofu/swf/parser/parsing/ParseTagBase.h"
#include "botofu/swf/parser/SwfReader.h"

struct ParseTagFileAttributes final : public ParseTagBase {

    virtual ~ParseTagFileAttributes() = default;

    std::shared_ptr<TagBase> operator()(SwfReader &reader, uint32 size) override;

};

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGFILEATTRIBUTES_H
