#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_MULTINAME_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_MULTINAME_H

#include <vector>
#include <memory>

#include "botofu/swf/parser/SwfReader.h"
#include "botofu/ios/types.h"
#include "botofu/swf/parser/abc/enumerations.h"


/**
 * CLASS representing an "name" (an identifiant) in the AMV2 specification.
 *
 * This class holds an identifiant and either a namespace or a namespace set.
 */
namespace abc {

    class ConstantPoolInfo;

    namespace multiname {

        /**
         * A base structure to all the MULTINAME's kinds structure to play with polymorphism.
         */
        struct BaseMultiname {

            virtual ~BaseMultiname() = default;

            /**
             * Return the name of the structure.
             * @return A string representation of the structure's name.
             */
            virtual std::string get_name() const = 0;

            /**
             * Get the string representation of the structure according to AMV2 specifications.
             * @param constant_pool The structure that contains the constants defined in the DOABC tag.
             * @return The string representation of the structure according to AMV2 specifications.
             */
            virtual std::string
            to_string(ConstantPoolInfo const &constant_pool) const = 0;
        };

        /** An empty class saying that the MULTINAME is not fully read at the moment. */
        struct NothingForTheMoment : BaseMultiname {
            NothingForTheMoment() = default;

            std::string get_name() const override { return "NothingForTheMoment"; }

            std::string
            to_string(ConstantPoolInfo const &) const override { return "NothingForTheMoment"; }
        };

        /** Qualified Name, a name with exactly one namespace. */
        struct QName : BaseMultiname {
            explicit QName(SwfReader &reader);

            std::string get_name() const override { return "QNAME"; }

            std::string
            to_string(ConstantPoolInfo const &constant_pool) const override;

            uint32 m_namespace;
            uint32 m_name;
        };

        /**
         * RunTime Qualified Name, a name with a runtime resolution of the namespace.
         *
         * The namespace that should be used by the RTQNAME will be on the top of the
         * stack so the RTQNAME will need to pop one namespace from the stack.
         *
         * Example:
         * var ns = getANamespace();
         * x = ns::r;
         * -> the RTQNAME created is "r".
         */
        struct RtqName : BaseMultiname {
            explicit RtqName(SwfReader &reader);

            std::string get_name() const override { return "RTQNAME"; }

            std::string
            to_string(ConstantPoolInfo const &constant_pool) const override;

            uint32 m_name;
        };

        /**
         * RunTime Qualified Name Late, a name with a runtime resolution of both the namespace and the name.
         *
         * The namespace and the name that should be used by the RTQNAMEL will be on the top of the stack
         * so the RTQNAMEL will need to pop 2 items from the stack.
         *
         * Example:
         * var x = getAName();
         * var ns = getANamespace();
         * w = ns::[x];
         */
        struct RtqNameL : BaseMultiname {
            std::string get_name() const override { return "RTQNAMEL"; }

            std::string
            to_string(ConstantPoolInfo const &) const override { return "RTQNAMEL"; }
        };

        /**
         * Multiple NAMESPACE Name, a name and multiple namespaces.
         */
        struct Multiname : BaseMultiname {
            explicit Multiname(SwfReader &reader);

            std::string get_name() const override { return "MULTINAME"; }

            std::string
            to_string(ConstantPoolInfo const &constant_pool) const override;

            uint32 m_name;
            uint32 m_namespace_set;
        };

        /**
         * Multiple NAMESPACE Name Late, a name resolved at runtime and multiple namespaces.
         */
        struct MultinameL : BaseMultiname {
            explicit MultinameL(SwfReader &reader);

            std::string get_name() const override { return "MULTINAMEL"; }

            std::string
            to_string(ConstantPoolInfo const &constant_pool) const override;

            uint32 m_namespace_set;
        };

        /**
         * This type is not described by the AMV2 documentation at the moment of writing.
         *
         * The possible interpretation for this type is that it represent templated types
         * (only types of the form Vector<[type]>).
         */
        struct TemplatedTypeMultiname : BaseMultiname {
            explicit TemplatedTypeMultiname(SwfReader &reader);

            std::string get_name() const override { return "TemplatedTypeMultiname"; }

            std::string
            to_string(ConstantPoolInfo const &constant_pool) const override;

            uint32              m_name;
            uint32              m_param_count;
            std::vector<uint32> m_param_names;
        };
    }

    /**
     * CLASS storing an abstract identifier.
     *
     * All the possible identifier types are stored in the structures in the namespace
     * "multiname".
     */
    struct Multiname {

        /**
         * Default constructor of the MULTINAME class.
         */
        Multiname();

        explicit Multiname(SwfReader &reader);

        std::string to_string(ConstantPoolInfo const &constant_pool) const;

        std::string get_kind_representation() const;

        MultinameKind                             m_kind;
        std::unique_ptr<multiname::BaseMultiname> m_data;
    };

} // abc

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_MULTINAME_H
