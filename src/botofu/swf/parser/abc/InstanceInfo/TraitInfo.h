#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_INSTANCEINFO_TRAITINFO_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_INSTANCEINFO_TRAITINFO_H

#include <istream>
#include <memory>

#include <boost/optional.hpp>

#include "botofu/ios/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/enumerations.h"
#include "botofu/swf/parser/abc/Metadata.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"

struct TagDoAbc;

namespace abc {

    namespace trait {

        struct TraitBase {

            virtual ~TraitBase() = default;

            virtual std::string
            to_string(TagDoAbc const &tag_do_abc, unsigned indent_level) const = 0;

            virtual std::string name(TagDoAbc const &tag_do_abc) const = 0;

            virtual std::string
            data(ConstantPoolInfo const &constant_pool_info) const;

            virtual std::string
            type(ConstantPoolInfo const &constant_pool_info) const;

            virtual uint32 index() const;
        };

        struct TraitSlot : TraitBase {

            explicit TraitSlot(SwfReader &reader);

            std::string
            to_string(TagDoAbc const &tag_do_abc, unsigned indent_level) const final;

            std::string name(TagDoAbc const &tag_do_abc) const final;

            std::string data(ConstantPoolInfo const &constant_pool_info) const final;

            std::string type(ConstantPoolInfo const &constant_pool_info) const final;

            uint32                             m_slot_id;
            uint32                             m_type_name;
            uint32                             m_vindex;
            boost::optional<abc::ConstantKind> m_vkind;
        };

        struct TraitClass : TraitBase {

            explicit TraitClass(SwfReader &reader);

            std::string
            to_string(TagDoAbc const &tag_do_abc, unsigned indent_level) const final;

            std::string name(TagDoAbc const &tag_do_abc) const final;

            uint32 m_slot_id;
            uint32 m_classi;
        };

        struct TraitFunction : TraitBase {

            explicit TraitFunction(SwfReader &reader);

            std::string
            to_string(TagDoAbc const &tag_do_abc, unsigned indent_level) const final;

            std::string name(TagDoAbc const &tag_do_abc) const final;

            uint32 m_slot_id;
            uint32 m_function;
        };

        struct TraitMethod : TraitBase {

            explicit TraitMethod(SwfReader &reader);

            std::string
            to_string(TagDoAbc const &tag_do_abc, unsigned indent_level) const final;

            std::string name(TagDoAbc const &tag_do_abc) const final;

            uint32 index() const final;

            uint32 m_disp_id;
            uint32 m_method;
        };

        struct TraitUnknown : TraitBase {
            std::string
            to_string(TagDoAbc const &tag_do_abc, unsigned indent_level) const final;

            std::string name(TagDoAbc const &tag_do_abc) const final;
        };

    } // trait

    struct TraitInfo {

        /**
         * Constructor of the TraitInfo class.
         * @param reader The reader from which we will read the informations needed to construct the class.
         */
        explicit TraitInfo(SwfReader &reader);

        /**
         * Helper method for getting a readable representation of the Trait kind.
         * @return The string (human-readable) representation of the kind of the TraitInfo instance.
         */
        std::string get_kind_representation() const;

        /**
         * GETTER for the TraitInfo kind.
         * @return The kind of the TraitInfo.
         */
        TraitKind get_kind() const;

        /**
         * GETTER for the name of the trait.
         * @param constant_pool The structure that store all the constants of the DOABC tag.
         * @return The name of the trait as a string (human-readable).
         */
        std::string get_name(ConstantPoolInfo const &constant_pool) const;

        /**
         * GETTER for the flag attributes of the trait.
         * @return A single byte with the first flag attribute on the less important bit.
         */
        uint8 get_flag_attributes() const;

        std::string get_data(ConstantPoolInfo const &constant_pool_info) const;

        std::string name(TagDoAbc const &tag_do_abc) const;

        std::string
        to_string(TagDoAbc const &tag_do_abc, unsigned indent_level) const;

        std::string get_type(ConstantPoolInfo const &constant_pool_info) const;

        uint32 get_index() const;

        bool is_final() const;

        bool is_override() const;

        bool has_metadata() const;

        bool is_getter() const;

        bool is_setter() const;

        bool is_method() const;

        bool is_function() const;

        bool is_class() const;

        bool is_const() const;

        bool is_slot() const;

        uint32                                 m_name;
        uint8                                  m_kind;
        std::unique_ptr<abc::trait::TraitBase> m_data;
        boost::optional<uint32>                m_metadata_count;
        boost::optional<std::vector<uint32> >  m_metadatas;

    };

} // abc

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_INSTANCEINFO_TRAITINFO_H
