target_sources(botofu_swf_parser PUBLIC ClassInfo.cpp ConstantPoolInfo.cpp InstanceInfo.cpp Metadata.cpp MethodBody.cpp MethodInfo.cpp ScriptInfo.cpp)
target_sources(botofu_swf_parser INTERFACE ClassInfo.h ConstantPoolInfo.h constants.h enumerations.h InstanceInfo.h Metadata.h MethodBody.h MethodInfo.h ScriptInfo.h)

add_subdirectory(ConstantPoolInfo)
add_subdirectory(InstanceInfo)
add_subdirectory(Metadata)
add_subdirectory(MethodBody)
add_subdirectory(MethodInfo)