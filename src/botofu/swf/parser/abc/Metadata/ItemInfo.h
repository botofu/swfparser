#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METADATA_ITEMINFO_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METADATA_ITEMINFO_H

#include <istream>

#include "botofu/ios/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"

namespace abc {

    struct ItemInfo {

        explicit ItemInfo(SwfReader &reader);

        std::string
        to_string(ConstantPoolInfo const &constant_pool, unsigned indent_level) const;

        uint32 m_key;
        uint32 m_value;
    };

} // abc

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METADATA_ITEMINFO_H
