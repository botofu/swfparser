#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METADATA_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METADATA_H

#include <istream>
#include <vector>
#include <string>

#include "botofu/ios/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/Metadata/ItemInfo.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"

namespace abc {

    struct Metadata {

        explicit Metadata(SwfReader &reader);

        std::string
        to_string(abc::ConstantPoolInfo const &constant_pool, unsigned indent_level) const;

        uint32                     m_name;
        uint32                     m_item_count;
        std::vector<abc::ItemInfo> m_items;
    };

} // abc

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METADATA_H
