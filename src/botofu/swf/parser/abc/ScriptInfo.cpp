#include "botofu/swf/parser/abc/ScriptInfo.h"

abc::ScriptInfo::ScriptInfo(SwfReader &reader) {
    m_init        = reader.read_var_u30();
    m_trait_count = reader.read_var_u30();
    for (uint32 i{0}; i < m_trait_count; ++i) {
        m_traits.emplace_back(reader);
    }
}