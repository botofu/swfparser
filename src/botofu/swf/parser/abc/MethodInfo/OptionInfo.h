#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_OPTIONINFO_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_OPTIONINFO_H

#include <vector>
#include <istream>

#include "botofu/ios/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/MethodInfo/OptionDetail.h"

namespace abc {

    struct OptionInfo {

        explicit OptionInfo(SwfReader &reader);

        uint32                    m_option_count;
        std::vector<OptionDetail> m_options;
    };

} // abc

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_OPTIONINFO_H
