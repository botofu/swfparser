#include "botofu/swf/parser/abc/MethodInfo/ParamInfo.h"

abc::ParamInfo::ParamInfo(SwfReader &reader, uint32 param_count) {
    for (uint32 i{0}; i < param_count; ++i) {
        param_names.push_back(reader.read_var_u30());
    }
}