#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_PARAMINFO_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_PARAMINFO_H

#include <istream>
#include <vector>

#include "botofu/ios/types.h"
#include "botofu/swf/parser/SwfReader.h"

namespace abc {

    struct ParamInfo {

        ParamInfo(SwfReader &reader, uint32 param_count);

        std::vector<uint32> param_names;

    };

} // abc

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_PARAMINFO_H
