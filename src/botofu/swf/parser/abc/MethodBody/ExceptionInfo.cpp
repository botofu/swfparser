#include "botofu/swf/parser/abc/MethodBody/ExceptionInfo.h"

abc::ExceptionInfo::ExceptionInfo(SwfReader &reader) {
    m_from     = reader.read_var_u30();
    m_to       = reader.read_var_u30();
    m_target   = reader.read_var_u30();
    m_exc_type = reader.read_var_u30();
    m_var_name = reader.read_var_u30();
}