#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_CLASSINFO_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_CLASSINFO_H

#include <istream>
#include <vector>

#include "botofu/ios/types.h"
#include "botofu/swf/parser/abc/InstanceInfo/TraitInfo.h"
#include "botofu/swf/parser/SwfReader.h"

namespace abc {

    struct ClassInfo {

        explicit ClassInfo(SwfReader &reader);

        std::string
        to_string(TagDoAbc const &tag_do_abc, unsigned indent_level) const;

        uint32                      m_cinit;
        uint32                      m_trait_count;
        std::vector<abc::TraitInfo> m_traits;
    };

} // abc

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_ABC_CLASSINFO_H
