#ifndef BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_SWFREADER_H
#define BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_SWFREADER_H

#include <cstddef>                     // std::size_t

#include <boost/endian/conversion.hpp> // Endianness related work
#include <boost/filesystem.hpp>

#include <iostream>
#include <iomanip>
#include <fstream>

#include "botofu/ios/BinaryReader.h"
#include "botofu/swf/parser/SwfStructs.h"

namespace fs = boost::filesystem;

class SwfReader : public LittleEndianReader {

public:

    explicit SwfReader(fs::path const &path, std::size_t header_size = 0);

    SwfReader(char const *data, std::size_t size);

    template <typename BaseType>
    SwfFixedPoint<BaseType> read_fixed_point() {
        SwfFixedPoint<BaseType> ret;
        this->read_and_endian_convert<BaseType>(ret.fractional_part);
        this->read_and_endian_convert<BaseType>(ret.decimal_part);
        return ret;
    }

    SwfRect read_rect();

    std::string read_null_terminated_utf();

    uint32 read_var_u30();

    int32 read_int24();

private:

    std::string m_decompressed_file;

};

#endif //BOTOFU_SWF_PARSER_SRC_BOTOFU_SWF_PARSER_SWFREADER_H
